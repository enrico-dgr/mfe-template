## Available Scripts

In the project directory, you can run:

### `npm run server`

Runs the server API in the development mode.

./index.ts is the entry and ts-node will compile everything in src and front-end directories into the build dir.
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.
The page will reload if you make edits.

### `npm run build`

Build src, index.ts, front-ent into corrispondent directories into ./build.

### `npm run start`

Will run the build dir.


import { Request, Response, NextFunction } from "express";
import jwt = require("jsonwebtoken");
import config = require("config");

import TokenInterface from "../models/User/UserJwt";
import IUser from '../models/User/UserDocument';

module.exports = function(req: Request, res: Response, next: NextFunction) {
  // Get token from header
  const token = req.header("x-auth-token");

  // Check if not token
  if (!token) {
    return res.status(401).json({ msg: "No token, authorization denied" });
  }

  // Verify token
  try {
    const decoded = jwt.verify(token, config.get("jwtSecret"));

    req.user = (decoded as TokenInterface).user as IUser;
    next();
  } catch (err) {
    res.status(401).json({ msg: "Token is not valid" });
  }
};

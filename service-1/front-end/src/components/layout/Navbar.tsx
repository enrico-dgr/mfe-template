import React from "react";

const Navbar = () => {
  return (
    <nav>
      <h1>
        <a href="index.html">
          <i>App</i>
        </a>
      </h1>
      <ul>
        <li>
          <a href="profiles.html">Developers</a>
        </li>
        <li>
          <a href="register.html">Register</a>
        </li>
        <li>
          <a href="login.html">Login</a>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;

import mongoose, { Schema } from "mongoose";
import IUser from './UserDocument';

const UserSchema: Schema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  avatar: { type: String },
  date: { type: Date, default: Date.now },
});

const User: mongoose.Model<IUser> = mongoose.model("user", UserSchema);
export default User;